<?php

namespace App\Controller\Backend\RecursosHumanos;

use App\Entity\RecursosHumanos\Area;
use App\Form\RecursosHumanos\AreaType;
use App\Repository\RecursosHumanos\AreaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/recursos_humanos/area")
 */
class AreaController extends AbstractController
{
    /**
     * @Route("/", name="backend_recursos_humanos_area_index", methods={"GET"})
     */
    public function index(AreaRepository $areaRepository): Response
    {
        return $this->render('backend/recursos_humanos/area/index.html.twig', [
            'areas' => $areaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="backend_recursos_humanos_area_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $area = new Area();
        $form = $this->createForm(AreaType::class, $area);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($area);
            $entityManager->flush();

            return $this->redirectToRoute('backend_recursos_humanos_area_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/recursos_humanos/area/new.html.twig', [
            'area' => $area,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="backend_recursos_humanos_area_show", methods={"GET"})
     */
    public function show(Area $area): Response
    {
        return $this->render('backend/recursos_humanos/area/show.html.twig', [
            'area' => $area,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="backend_recursos_humanos_area_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Area $area, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AreaType::class, $area);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('backend_recursos_humanos_area_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/recursos_humanos/area/edit.html.twig', [
            'area' => $area,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="backend_recursos_humanos_area_delete", methods={"POST"})
     */
    public function delete(Request $request, Area $area, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$area->getId(), $request->request->get('_token'))) {
            $entityManager->remove($area);
            $entityManager->flush();
        }

        return $this->redirectToRoute('backend_recursos_humanos_area_index', [], Response::HTTP_SEE_OTHER);
    }
}
