<?php

namespace App\Form\RecursosHumanos;

use App\Entity\RecursosHumanos\Area;
use App\Entity\RecursosHumanos\Empleado;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EmpleadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $perteneceRH = $options['pertenece_rh'];

        $builder
            ->add('nombre');
        if($perteneceRH) {
            $builder->add('area', EntityType::class, [
                'class' => Area::class,
                'placeholder' => 'Seleccione...'
            ]); //[Objetos de tipo area]
        }
           

            $builder
            ->add('fechaNacimiento')
            ->add('activo')
            
            
            
            ->add('numeroEmpleado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Empleado::class,
            'attr' => ['novalidate' => true],
            'pertenece_rh' => false,
        ]);
    }
}
