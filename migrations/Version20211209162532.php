<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211209162532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE recursos_humanos.empleado ADD area_id INT NOT NULL');
        $this->addSql('ALTER TABLE recursos_humanos.empleado ADD CONSTRAINT FK_9EC9B2CFBD0F409C FOREIGN KEY (area_id) REFERENCES recursos_humanos.area (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9EC9B2CFBD0F409C ON recursos_humanos.empleado (area_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        #$this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE recursos_humanos.empleado DROP CONSTRAINT FK_9EC9B2CFBD0F409C');
        $this->addSql('DROP INDEX IDX_9EC9B2CFBD0F409C');
        $this->addSql('ALTER TABLE recursos_humanos.empleado DROP area_id');
    }
}
